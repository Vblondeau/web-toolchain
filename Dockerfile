FROM jetty:jre8

ARG REDIS_ADDR="146.148.25.128"

USER root
RUN mkdir /app
WORKDIR /app

RUN apt-get update -y && apt-get  install -y \
	maven \
	openjdk-8-jdk \
&& apt-get clean -y
RUN chown jetty:jetty -R /app
COPY click-count /app/click-count
#RUN wget  https://github.com/xebia-france/click-count/tarball/master && tar -xzf master && rm master
#RUN mv xebia* click-count

RUN sed -i "s|redisHost = \"redis\";|redisHost = \"$REDIS_ADDR\";|g" click-count/src/main/java/fr/xebia/clickcount/Configuration.java
RUN cd click-count && mvn clean package
RUN mv click-count/target /var/lib/jetty/webapps 
#RUN mv click-count/*  /var/lib/jetty/webapps 
#WORKDIR /var/lib/jetty
#RUN mkhomedir_helper jetty
RUN chown -R jetty:jetty  /var/lib/jetty/webapps
USER jetty
#RUN cd webapps && mvn clean package
WORKDIR /var/lib/jetty
