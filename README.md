## Toolchain hébergée sur un cluster k8s.


Gitlab dispose maintenant d'un intégratio native avec k8s, permettant de déployer directement les containers de CI.

Ce repo est connecté à un cluter k8s. Il est également possible de définir son propre runner.


# les objectifs de cette toolchain
Cette toolchain a pour objectif de :
- construire une image docker contenant l'appli
- la déployer sur un cluter k8s (1 seul replicas) afin de vérifier que le service est fonctionnel
- tester le service
- déployer le service en production si on push sur master (2 réplicas, processus de rolling upgrade -> pas d'interruption de service)


Le a chaîne CI utilise des variables d'environnements propres au compte gitlab:

Paramétres à renseigner dans les variables d'environnemetn CI: 
- DOCKER_LOGIN : votre username dockerhub
- DOCKER_PASSWD: votre password dockerhub
- SERVICE_ACCOUNT: une clé pour se connecter à un compte google

Si on veut mettre ça en place en entreprise, il y a des opérations supplémentaires:

- définir des namespaces k8s spécifiques pour les containers CI, DEV et prod

- compléter les tests

Actuellement, l'adresse du serveur redis est définie à la construction du container. On a un container de prod et un de dev, avec comme différence l'adresse du redis

On vérifie que les serveurs renseignés fonctionnent avant de pousser en prod






### Annexes

#### Le serveur redis

actuellment, le serveur redis est un container hébergé sur le cluster k8s.

#### adresse des appli-web déployées :
test:
http://35.195.71.70:8080/target/clickCount/

prod: 
http://104.199.108.44:8080/target/clickCount/

utilisation de gitlab avec gitlab-CI pour le déploiement.



#### Mettre en place un serveur gitlab:

```
docker run --detach \
	--hostname gitlab.example.com \
	--publish 443:443 --publish 80:80 --publish 22:22 \
	--name gitlab \
	--restart always \
	--volume /srv/gitlab/config:/etc/gitlab \
	--volume /srv/gitlab/logs:/var/log/gitlab \
	--volume /srv/gitlab/data:/var/opt/gitlab \
	gitlab/gitlab-ce:latest
```