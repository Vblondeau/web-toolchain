#!/bin/bash
echo "waiting to be sure that app have enough time to be deployed"
sleep 60
out=$(wget --spider -S "http://35.195.71.70:8080/target/clickCount/" 2>&1 | grep "HTTP/" | awk '{print $2}')
if [[ $out == "200" ]]
then
  echo "web interface is up and running"
  exit 0
else
    echo "web interface unreachable"
    exit 1
fi