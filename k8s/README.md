pour faire fonctionner la pipeline, il est nécessaire de faire le premier setup à la main.

Les fichiers suivants permettent de déployer la "prod" sur un cluster k8s.
utilisez depuis ce dossier:
```
kubectl create -f deployment_prod.yml
kubectl create -f service.yml
```