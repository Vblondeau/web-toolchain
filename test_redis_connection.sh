#!/bin/bash
REDIS_HOST=$1
if [ -z $2 ]
 then 
 REDIS_PORT=6379
 else
 REDIS_PORT=$2
fi


{ echo "ping";  sleep 1; echo -e '\x1dclose\x0d';} | telnet $REDIS_HOST   $REDIS_PORT
result_code=$?
if [[ $result_code == 0 ]]
then 
    echo "redis connection is ok, continuing operation"
else
    echo "redis seems not to be up. did you give a valid adress?"
    exit 1
fi